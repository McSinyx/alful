# alful

This six-line extension makes Firefox Quantum open all windows in fullscreen
to hide the toolbars in windowed mode (`full-screen-api.ignore-widgets = true`).
All credits go to tazeat, who wrote the original version and suggested
[the change to achieve the current behavior](https://github.com/tazeat/AutoFullscreen/issues/4#issuecomment-509723353).
