function fullscreen (window) {
    browser.windows.update(window.id, {state: "fullscreen"})
}

browser.windows.getAll().then(windows => windows.forEach(fullscreen));
browser.windows.onCreated.addListener(fullscreen);
